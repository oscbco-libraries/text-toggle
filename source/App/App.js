import React, {PureComponent} from 'react';

import TextToggle from '../TextToggle/TextToggle';
import styles from './_App.scss';

export default class App extends PureComponent {
  render () {
    return (
      <div id={styles.app}>
        <div>
          <TextToggle leftOption='LEFT' rightOption='RIGHT' />
        </div>
      </div>
    );
  }
}
